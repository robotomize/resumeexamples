<?php

namespace Utils;

include 'Vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

/**
 * @author robotomize@gmail.com
 * @version 0.1.1
 * @usage $tForm = new FormBuilder(['first_name' => 'my', 'last_name' => 'name', 'third_name' => 'is', 'email' => 'vasya']);
 * Class FormBuilder
 */
class FormBuilder
{
    /**
     * @var string
     */
    private $_lastName = '';
    /**
     * @var string
     */
    private $_firstName = '';
    /**
     * @var string
     */
    private $_thirdName = '';
    /**
     * @var string
     */
    private $_email = '';

    /**
     * @param $input string
     * @return string
     */
    public function inputValidate($input)
    {
        $filtered = stripcslashes(trim($input));
        $filtered = strip_tags($filtered);
        $filtered = htmlspecialchars($filtered);
        return $filtered;
    }

    /**
     * @return string
     */
    public function getFName()
    {
        return $this->_firstName;
    }

    /**
     * @return string
     */
    public function getLname()
    {
        return $this->_lastName;
    }

    /**
     * @return string
     */
    public function getTName()
    {
        return $this->_thirdName;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->_email;
    }

    /**
     * @param $data array
     */
    function __construct($data)
    {
        $this->_firstName = (!empty($data['first_name']) ? $this->inputValidate($data['first_name']) : 'Empty name');

        $this->_lastName = (!empty($data['last_name']) ? $this->inputValidate($data['last_name']) : 'Empty last name');

        $this->_thirdName = (!empty($data['third_name']) ? $this->inputValidate($data['third_name']) : 'Empty third name');

        $this->_email = (!empty($data['email']) ? $this->inputValidate($data['email']) : 'Empty email');

        $this->_email  = filter_var($this->_email , FILTER_VALIDATE_EMAIL);
    }
}

/**
 * twig шаблон для отладки
 */
try {
    $loader = new \Twig_Loader_Filesystem('.');
    $twig = new \Twig_Environment($loader);

    if (!empty($_POST)) {
        $formObject = new FormBuilder($_POST);
        $dump_variable = sprintf('%s %s %s %s', $formObject->getFName(), $formObject->getEmail(), $formObject->getLName(), $formObject->getTName());
        /**
         * Отладочную информацию выводим в html в twig шаблон
         */
        print $twig->render('form.html',
            ['debug_data' => $dump_variable]
        );
    } else {
        print $twig->render('form.html', ['debug_data' => 'empty data']);
    }

    /**
     * Пишем через монолог, логирование информации с формы
     */
    $log = new Logger('From Form');
    $log->pushHandler(new StreamHandler('msg.txt', Logger::WARNING));
    $log->addWarning($dump_variable);
} catch (Exception $e) {
    print_r($e);
}
