﻿# ************************************************************
# Sequel Pro SQL dump
# Версия 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Адрес: localhost (MySQL 5.6.26)
# Схема: test_db
# Время создания: 2015-09-14 02:27:04 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Дамп таблицы dict_vacations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `dict_vacations`;

CREATE TABLE `dict_vacations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

LOCK TABLES `dict_vacations` WRITE;
/*!40000 ALTER TABLE `dict_vacations` DISABLE KEYS */;

INSERT INTO `dict_vacations` (`id`, `title`)
VALUES
	(1,'Учебный'),
	(2,'За свой счет'),
	(3,'Оплачиваемый');

/*!40000 ALTER TABLE `dict_vacations` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы staff
# ------------------------------------------------------------

DROP TABLE IF EXISTS `staff`;

CREATE TABLE `staff` (
  `id_user` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `c_lastname` varchar(255) DEFAULT '',
  `c_name` varchar(255) DEFAULT '',
  `c_patronymic` varchar(255) DEFAULT '',
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

LOCK TABLES `staff` WRITE;
/*!40000 ALTER TABLE `staff` DISABLE KEYS */;

INSERT INTO `staff` (`id_user`, `c_lastname`, `c_name`, `c_patronymic`)
VALUES
	(1,'Иванов','Никита','Алексеевич'),
	(2,'Петров','Иван','Сидорович'),
	(3,'Пинясов','Владимир','Олегович');

/*!40000 ALTER TABLE `staff` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы staff_vacation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `staff_vacation`;

CREATE TABLE `staff_vacation` (
  `vac_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `d_start` date DEFAULT NULL,
  `d_finish` date DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  PRIMARY KEY (`vac_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

LOCK TABLES `staff_vacation` WRITE;
/*!40000 ALTER TABLE `staff_vacation` DISABLE KEYS */;

INSERT INTO `staff_vacation` (`vac_id`, `id_user`, `d_start`, `d_finish`, `type`)
VALUES
	(1,1,'2011-01-20','2011-01-25',3),
	(2,1,'2011-03-11','2011-03-30',3),
	(3,1,'2011-06-15','2011-06-20',3),
	(4,2,'2011-07-03','2011-07-30',3),
	(5,3,'2011-08-01','2011-08-31',3),
	(6,1,'2013-08-01','2013-08-31',3);

/*!40000 ALTER TABLE `staff_vacation` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
