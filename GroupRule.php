<?php

namespace app\components\rbac;
 
use Yii;
use yii\rbac\Rule;
 
/**
 * User group rule class.
 */
class GroupRule extends Rule
{
        /**
         * @inheritdoc
         */
        public $name = 'group';
 
        /**
         * @inheritdoc
         */
        public function execute($user, $item, $params)
        {
                if (!Yii::$app->user->isGuest) {
                        $roles = Yii::$app->authManager->getRolesByUser($user);
 
                        if ($item->name === 'admin') {
                                return isset($roles[$item->name]);
                        } elseif ($item->name === 'user') {
                                return isset($roles[$item->name]) || isset($roles['admin']);
                        }
                }
                return false;
        }
}
