/**
Example3 дамп структуры таблиц для задания example3_dump.sql
Сформировать SQL запрос. Имеется 3 таблицы staff (сотрудники), staff_vacation (отпуска сотрудников), dict_vacations (справочник названий отпусков). 
Необходимо вывести список сотрудников отгулявших в 2011 году оплачиваемый отпуск более  28 дней.
Решение 
 */

SET @our_year = 2011;
SET @vacation = 28;
SELECT `staff`.`c_lastname` as 'Фамилия', `staff`.`c_name` as 'Имя', `staff`.`c_patronymic` as 'Отчество', sum(TO_DAYS(d_finish) - TO_DAYS(d_start)) as 'Дней отпуска' from `staff_vacation`
  inner join `staff` on `staff`.`id_user` = `staff_vacation`.`id_user`
    where `staff_vacation`.`type` = 3 and YEAR(d_finish) = @our_year and YEAR(d_start) = @our_year
    group by `staff_vacation`.`id_user` having sum(TO_DAYS(d_finish) - TO_DAYS(d_start)) > @vacation 