# README #

* Примерная документация по проекту тестового задания.
* **Проект обновлен**
### Краткая аннотация ###

* Проект без тестов
* Version 1
* По заданию 4
* * **Обновлено example4.sql**
* ПО заданию 5
* * RbacController.php консольный контролер, инициализируем роли, 4 роли, я использовал в итоге 2
* * GroupRule.php класс для проверки роли текущего юзера
* * web.php 
* * В BooksController описано поведение для экшенов, index доступен всем не авторизованным, view index всем авторизованным и все остальное edit, create только админу
* **Учетки**
* * **demo demo**
* * **admin admin** 

### Установка ###
* **git clone https://robotomize@bitbucket.org/robotomize/resumeexamples.git**
* Установка composer если нет
* curl -sS https://getcomposer.org/installer | php
* mv composer.phar /usr/local/bin/composer
* composer update
* profit