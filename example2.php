<?php

namespace Utils;
/**
 * @author robotomize@gmail.com
 * @version 0.1.2
 * @usage
 *  $t = new ArrayManipulation(15, 84); // выбираем диапазон
 *  $t->massiveMoveRandomEl(); на экране сразу выведется 20 раз массив
 *  $t->setMinRange(1);
 *  $t->setMaxRange(158);
 *  массив будет состоять из рандомных элементов в диапазоне от 1 до 158
 *  можно увеличить или уменьшить размерность массива
 *  $t->setMaxSlice(15);
 *  размерность массива $_shuffleArray станет 15 элементов
 *  можно вывести массив только один раз
 *  $t->pickAndMoveRandomEl();
 *  или изменить количество операций над массивом
 *  $t->setCountPick(10);
 *  $t->massiveMoveRandomEl();
 *  операция выполнится 10 раз
 * Class ArrayManipulation
 */
class ArrayManipulation
{
    /** range 15-84
     * @var int
     */
    private $_minRange = 0;

    /**
     * @var int
     */
    private $_maxRange = 0;

    /**
     * Наш массив с которым работает
     * @var array
     */
    private $_shuffleArray = [];

    /** Отрезаем нам массив от 0 до 10 или другие параметры можно установить через сеттеры
     * @var int
     */
    private $minSlice = 0;

    /**
     * @var int
     */
    private $maxSlice = 10;

    /** Диапазон значений для генерации рендомного ключа
     * @var int
     */
    private $maxRandRange;

    /**
     * @var int
     */
    private $minRandRange;

    /**
     * Гетеры сеттеры для задания, получения какой по длине массив хотим сгенерировать
     *
     */

    /**
     * @return int
     */
    public function getMinSlice()
    {
        return $this->minSlice;
    }


    /**
     * @return int
     */
    public function getMaxSlice()
    {
        return $this->maxSlice;
    }

    /**
     * @param int $maxSlice
     */
    public function setMaxSlice($maxSlice)
    {
        $this->maxSlice = $maxSlice;
        $this->maxRandRange = $this->maxSlice - 1;
        $this->minRandRange = $this->minSlice;
        $this->generateRand();
    }

    /**
     * @return int
     */
    public function getMaxRange()
    {
        return $this->_maxRange;
    }

    /**
     * @param int $maxRange
     */
    public function setMaxRange($maxRange)
    {
        $this->_maxRange = $maxRange;
        $this->generateRand();
    }

    /**
     * @return int
     */
    public function getMinRange()
    {
        return $this->_minRange;
    }

    /**
     * @param int $minRange
     */
    public function setMinRange($minRange)
    {
        $this->_minRange = $minRange;
        $this->generateRand();
    }

    /**
     * @usage задаем диапазон значений которые хотим видеть в массиве через range и
     * отрезаем кусок массива
     */
    private function generateRand()
    {
        $numbers = range($this->_minRange, $this->_maxRange);
        shuffle($numbers);
        $this->_shuffleArray = array_slice($numbers, $this->minSlice, $this->maxSlice);
    }

    /**
     * @usage функция вывода нашего массива, дергаем ее когда необходимо
     */
    public function viewOurArray()
    {
        if (!empty($this->_shuffleArray)) {
            foreach ($this->_shuffleArray as $value) {
                print $value . PHP_EOL;
            }
        }
        print 'finished..' . PHP_EOL;
    }

    /**
     * сохраняем ссылку на текущий элемент
     * @var string
     */
    private $_currentElement;

    /**
     * индекс последнего элемента массива
     * @var int
     */
    private $_endElement;

    /**
     * текущий размер массива
     * @var int
     */
    private $_shuffleArraySize;

    /**
     * @usage функция выбирает случайные элемент, если он делится на 3 без
     * остатка то сдвигает его в начало, если нет то в конец
     */
    public function pickAndMoveRandomEl()
    {
        // выбираем элемент
        $key = rand($this->minRandRange, $this->maxRandRange);
        /**
         * прокидываем ссылки на нужные элементы и индексы
         */
        $this->_currentElement = $this->_shuffleArray[$key];
        $this->_shuffleArraySize = count($this->_shuffleArray);
        $this->_endElement = $this->_shuffleArraySize - 1;

        if ($this->_shuffleArray[$key] % 3 == 0) {
            // вставляем элемент на первое место, увеличивается количество элементов, поэтому  сдвигать нужно с key+1
            array_splice($this->_shuffleArray, 0, 0, $this->_currentElement );
            for($i = $key + 1; $i < $this->_shuffleArraySize; $i++) {
                // сдвигаем элементы
                $this->_shuffleArray[$i] = $this->_shuffleArray[$i + 1];
            }
            array_pop($this->_shuffleArray);
        } else {
            for($i = $key; $i < $this->_shuffleArraySize; $i++) {
                $this->_shuffleArray[$i] = $this->_shuffleArray[$i + 1];
            }
            $this->_shuffleArray[$this->_endElement] = $this->_currentElement ;
        }
        // ставим флаг, что мы уже меняли массив, нужен для магических методов
        $this->_arrayChange = true;
    }

    /**
     * @var int
     */
    private $countPick = 20;

    /**
     * Гетеры сеттеры чтобы менять количество операций над массивом, по умолчанию 20
     * @return int
     */
    public function getCountPick()
    {
        return $this->countPick;
    }

    /**
     * @param int $countPick
     */
    public function setCountPick($countPick)
    {
        $this->countPick = $countPick;
    }

    /**
     * @usage вызываем функцию выбора случайного элемента столько раз сколько зададим
     */
    public function massiveMoveRandomEl()
    {
        for($i = 0; $i < $this->countPick; $i++) {
            $this->pickAndMoveRandomEl();
            $this->viewOurArray();
        }
    }

    /**
     * @var bool
     */
    private $_arrayChange = false;

    /**
     * Если обратимся как к строке выведем сериализацию нашего текущего массива
     * @return string
     */
    function __toString()
    {
        if ($this->_arrayChange) {
            return serialize($this->_shuffleArray);
        } else {
            $this->pickAndMoveRandomEl();
            return serialize($this->_shuffleArray);
        }
    }

    /**
     * Если обратимся как к функции сделаем дамп текущего массива
     */
    public function __invoke()
    {
        if ($this->_arrayChange) {
            print_r($this->_shuffleArray);
        } else {
            $this->pickAndMoveRandomEl();
            print_r($this->_shuffleArray);
        }
    }

    /**
     * @return array
     */
    public function getShuffleArray()
    {
        return $this->_shuffleArray;
    }

    function __construct($min = 0, $max = 0)
    {
        if (empty($min) || empty($max) || $min == 0 || $max == 0) {
            throw new Exception('Required parameters error. Usage new ArrayManipulation(15, 84) for example');
        } else {
            $this->_minRange = $min;
            $this->_maxRange = $max;
            $this->maxRandRange = $this->maxSlice - 1;
            $this->minRandRange = $this->minSlice;
            $this->generateRand();
        }
    }
}



try {
    $t = new ArrayManipulation(15, 84);
    //$t->setCountPick(3);
    //$t->setMaxRange(158);
    //$t->setMinRange(0);
    $t->massiveMoveRandomEl();
} catch (Exception $e) {
    print_r($e);
}

